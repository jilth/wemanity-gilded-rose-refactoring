# Refactoring Gilded Rose

**requirement to run this project**

- yarn or npm
- node

# Installing

`yarn install`
or
`npm install`

# For yarn users

`yarn buildAndStart` to compile and start the program.
`yarn test` to start the test suit
`yarn build`to compile typescript files
`yarn start`to run the program with node

# For npm users

`npm run buildAndStart` to compile and start the program.
`npm run test` to start the test suit
`npm run build`to compile typescript files
`npm run start`to run the program with node
