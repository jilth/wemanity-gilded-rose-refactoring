import { Item } from './item'

export type HandleQuality = (item: Item) => number
export type HandleSellIn = (item: Item) => number
export type HandleBounds = (item: Item) => number

type ItemRuleOptions = {
  handleQuality?: HandleQuality
  handleSellIn?: HandleSellIn
  handleBounds?: HandleBounds
}

export class SpecialItemRule {
  rgxMatch: RegExp
  handleQuality: HandleQuality | undefined
  handleSellIn: HandleSellIn | undefined
  handleBounds: HandleBounds | undefined
  constructor(rgxMatch: RegExp, options: ItemRuleOptions) {
    this.rgxMatch = rgxMatch
    this.handleQuality = options.handleQuality
    this.handleSellIn = options.handleSellIn
    this.handleBounds = options.handleBounds
  }
}
