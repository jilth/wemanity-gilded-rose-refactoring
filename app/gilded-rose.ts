import { Item } from './item'
import { specialItemRules } from './rules'
import { HandleBounds, HandleQuality, HandleSellIn } from './special_item_rule'

type Handles = {
  handleQuality: HandleQuality
  handleSellIn: HandleSellIn
  handleBounds: HandleBounds
}

export class GildedRose {
  items: Array<Item>
  readonly defaultHandles: Handles

  constructor(items = [] as Array<Item>) {
    this.items = items
    this.defaultHandles = {
      handleQuality: this.defaultQualityHandle,
      handleSellIn: this.defaultSellInHandle,
      handleBounds: this.defaultBoundsHandle
    }
  }

  defaultQualityHandle(item: Item) {
    const decr = item.sellIn < 0 ? 2 : 1
    return item.quality - decr
  }
  defaultSellInHandle(item: Item) {
    return item.sellIn - 1
  }
  defaultBoundsHandle(item: Item) {
    if (item.quality > 50) return 50
    if (item.quality < 0) return 0
    return item.quality
  }

  getHandles(item: Item) {
    const handles = {} as Handles
    for (const itemRule of specialItemRules) {
      if (item.name.match(itemRule.rgxMatch)) {
        if (itemRule.handleQuality)
          handles.handleQuality = itemRule.handleQuality
        if (itemRule.handleSellIn) handles.handleSellIn = itemRule.handleSellIn
        if (itemRule.handleBounds) handles.handleBounds = itemRule.handleBounds
      }
    }
    return { ...this.defaultHandles, ...handles }
  }

  handleItem(item: Item) {
    const { handleQuality, handleSellIn, handleBounds } = this.getHandles(item)
    item.quality = handleQuality(item)
    item.sellIn = handleSellIn(item)
    item.quality = handleBounds(item)
  }
  updateQuality() {
    for (const item of this.items) {
      this.handleItem(item)
    }
  }
}
