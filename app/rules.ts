import { SpecialItemRule } from './special_item_rule'

const agedBrieRule = new SpecialItemRule(/^Aged Brie$/, {
  handleQuality: item => {
    const incr = item.sellIn <= 0 ? 2 : 1
    return item.quality + incr
  }
})
const BackstageRule = new SpecialItemRule(/^Backstage passes/, {
  handleQuality: item => {
    let quality = item.quality
    if (item.sellIn < 0) return 0
    if (item.sellIn <= 10) quality += 1
    if (item.sellIn <= 5) quality += 1
    return quality + 1
  }
})
const SulfurasRule = new SpecialItemRule(/^Sulfuras, Hand of Ragnaros$/, {
  handleQuality: item => item.quality,
  handleSellIn: item => item.sellIn,
  handleBounds: item => item.quality
})
const ConjuredRule = new SpecialItemRule(/^Conjured/, {
  handleQuality: item => item.quality - 2
})
export const specialItemRules = [
  agedBrieRule,
  BackstageRule,
  SulfurasRule,
  ConjuredRule
]
