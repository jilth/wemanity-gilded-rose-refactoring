import { GildedRose } from '../app/gilded-rose'
import { Item } from '../app/item'
import { SpecialItemRule } from '../app/special_item_rule'

const items = [
  new Item('foo', 0, 0),
  new Item('Power-up', 10, 18),
  new Item('Sulfuras, Hand of Ragnaros', 0, 80)
]

describe('Gilded Rose', function() {
  it('should have set defaultHandles', () => {
    const gildedRose = new GildedRose(items)
    expect(gildedRose.defaultHandles.handleQuality).toBe(
      gildedRose.defaultQualityHandle
    )
    expect(gildedRose.defaultHandles.handleSellIn).toBe(
      gildedRose.defaultSellInHandle
    )
    expect(gildedRose.defaultHandles.handleBounds).toBe(
      gildedRose.defaultBoundsHandle
    )
  })

  describe('::GildedRose default handle operations', () => {
    it('.defaultQualityHandle should decrement quality by 1', () => {
      const quality = 3
      const gildedRose = new GildedRose([new Item('testItem', 15, quality)])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].quality).toBe(quality - 1)
    })

    it('.defaultSellInHandle should decrement sellIn by 1', () => {
      const sellIn = 5
      const gildedRose = new GildedRose([new Item('testItem', sellIn, 5)])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].sellIn).toBe(sellIn - 1)
    })

    it('.defaultBoundsHandle should limit quality to 50', () => {
      const quality = 80
      const gildedRose = new GildedRose([new Item('testItem', -1, quality)])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].quality).toBe(50)
    })

    it('.defaultBoundsHandle should disallow negative quality', () => {
      const quality = -5
      const gildedRose = new GildedRose([new Item('testItem', -1, quality)])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].quality).toBe(0)
    })

    describe('when item sellIn is < 0', () => {
      it('.defaultQualityHandle should decrement by 2', () => {
        const quality = 3
        const gildedRose = new GildedRose([new Item('testItem', -1, quality)])
        gildedRose.updateQuality()
        expect(gildedRose.items[0].quality).toBe(quality - 2)
      })
    })
  })

  describe('::GildedRose special item rules', () => {
    it('should increment Aged Brie by 1', () => {
      const quality = 5
      const gildedRose = new GildedRose([new Item('Aged Brie', 5, quality)])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].quality).toBe(quality + 1)
    })
    it('should never decrease Sulfuras quality or sellIn', () => {
      const quality = 77
      const sellIn = -12
      const gildedRose = new GildedRose([
        new Item('Sulfuras, Hand of Ragnaros', sellIn, quality)
      ])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].quality).toBe(quality)
      expect(gildedRose.items[0].sellIn).toBe(sellIn)
    })
    it('should decrement Conjured items by 2', () => {
      const quality = 4
      const gildedRose = new GildedRose([new Item('Conjured toy', 5, quality)])
      gildedRose.updateQuality()
      expect(gildedRose.items[0].quality).toBe(quality - 2)
    })

    describe('when item sellIn is <= 10 and > 5', () => {
      it('should increment Backstage passes quality by 2', () => {
        const quality = 5
        const gildedRose = new GildedRose([
          new Item('Backstage passes to Boogie', 8, quality)
        ])
        gildedRose.updateQuality()
        expect(gildedRose.items[0].quality).toBe(quality + 2)
      })
    })
    describe('when item sellIn is <= 5', () => {
      it('should increment Backstage passes quality by 3', () => {
        const quality = 5
        const gildedRose = new GildedRose([
          new Item('Backstage passes to Boogie', 3, quality)
        ])
        gildedRose.updateQuality()
        expect(gildedRose.items[0].quality).toBe(quality + 3)
      })
    })
    describe('when item sellIn is < 0', () => {
      it('should set Backstage passes quality to 0', () => {
        const quality = 5
        const gildedRose = new GildedRose([
          new Item('Backstage passes to Boogie', -1, quality)
        ])
        gildedRose.updateQuality()
        expect(gildedRose.items[0].quality).toBe(0)
      })
    })

    describe('when item sellIn is <= 0', () => {
      it('should increment Aged Brie quality by 2', () => {
        const quality = 5
        const gildedRose = new GildedRose([new Item('Aged Brie', -1, quality)])
        gildedRose.updateQuality()
        expect(gildedRose.items[0].quality).toBe(quality + 2)
      })
    })
  })
})
